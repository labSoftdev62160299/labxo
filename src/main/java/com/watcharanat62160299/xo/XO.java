/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.watcharanat62160299.xo;

/**
 *
 * @author ACER
 */
import java.util.Scanner;

public class XO {

    public static void main(String[] args) {
        String[][] board = new String[4][4];
        board[0][0] = "";
        board[0][1] = "1";
        board[0][2] = "2";
        board[0][3] = "3";
        board[1][0] = "1";
        board[2][0] = "2";
        board[3][0] = "3";
        Scanner kb = new Scanner(System.in);
        try {

            boolean win = false;
            String firstturn;
            int xoturn = 0;
            int count = 0;
            int check1 = 0;

            System.out.println("Welcome to OX Game");
            while (check1 != 1) {
                System.out.println("X or O for First Turn");
                System.out.print(":");
                System.out.println("");
                firstturn = kb.next();
                System.out.println();

                if (firstturn.equals("X")) {
                    xoturn = 1;
                    check1 = 1;
                } else if (firstturn.equals("O")) {
                    xoturn = 2;
                    check1 = 1;
                } else {
                    System.out.println("Please enter X or O ");
                }

            }

            while (win != true) {
                System.out.print(" " + board[1][0] + " " + board[2][0] + " " + board[3][0]);
                System.out.println();
                for (int i = 0; i < 3; i++) {
                    if (i == 0) {
                        System.out.print(board[0][1]);
                    } else if (i == 1) {
                        System.out.print(board[0][2]);
                    } else if (i == 2) {
                        System.out.print(board[0][3]);
                    }

                    for (int j = 1; j < 4; j++) {

                        if (board[i + 1][j] == null || board[i + 1][j] == null || board[i + 1][j] == null) {
                            System.out.print("- ");
                        } else {
                            System.out.print(board[i + 1][j] + " ");
                        }
                    }
                    System.out.println("");
                }
                if (xoturn % 2 == 1) {
                    System.out.println("X turn");
                } else {
                    System.out.println("O turn");
                }

                boolean check2 = false;
                while (check2 != true) {
                    System.out.print("Please input Row col :");
                    System.out.println("");
                    int row = kb.nextInt();
                    int col = kb.nextInt();

                    if (row < 4 && col < 4) {
                        if (xoturn % 2 == 1 && board[row][col] == null) {
                            board[row][col] = "X";
                            xoturn++;
                            count++;
                            check2 = true;
                        } else if (xoturn % 2 == 0 && board[row][col] == null) {
                            board[row][col] = "O";
                            xoturn++;
                            count++;
                            check2 = true;
                        } else {
                            System.out.println("no empty");
                            System.out.println();

                        }
                    } else {
                        System.out.println("Error Please input again");
                        System.out.println();
                    }
                }

                if (board[1][1] == "X" && board[1][2] == "X" && board[1][3] == "X"
                        || board[2][1] == "X" && board[2][2] == "X" && board[2][3] == "X"
                        || board[3][1] == "X" && board[3][2] == "X" && board[3][3] == "X"
                        || board[1][1] == "X" && board[2][1] == "X" && board[3][1] == "X"
                        || board[1][2] == "X" && board[2][2] == "X" && board[3][2] == "X"
                        || board[1][3] == "X" && board[2][3] == "X" && board[3][3] == "X"
                        || board[1][1] == "X" && board[2][2] == "X" && board[3][3] == "X"
                        || board[1][3] == "X" && board[2][2] == "X" && board[3][1] == "X") {
                    System.out.print(" " + board[1][0] + " " + board[2][0] + " " + board[3][0]);
                    System.out.println();
                    for (int i = 0; i < 3; i++) {
                        if (i == 0) {
                            System.out.print(board[0][1]);
                        } else if (i == 1) {
                            System.out.print(board[0][2]);
                        } else if (i == 2) {
                            System.out.print(board[0][3]);
                        }

                        for (int j = 1; j < 4; j++) {

                            if (board[i + 1][j] == null || board[i + 1][j] == null || board[i + 1][j] == null) {
                                System.out.print("- ");
                            } else {
                                System.out.print(board[i + 1][j] + " ");
                            }
                        }
                        System.out.println("");
                    }
                    System.out.println("Player X win");
                    win = true;
                } else {
                    if (board[1][1] == "O" && board[1][2] == "O" && board[1][3] == "O"
                            || board[2][1] == "O" && board[2][2] == "O" && board[2][3] == "O"
                            || board[3][1] == "O" && board[3][2] == "O" && board[3][3] == "O"
                            || board[1][1] == "O" && board[2][1] == "O" && board[3][1] == "O"
                            || board[1][2] == "O" && board[2][2] == "O" && board[3][2] == "O"
                            || board[1][3] == "O" && board[2][3] == "O" && board[3][3] == "O"
                            || board[1][1] == "O" && board[2][2] == "O" && board[3][3] == "O"
                            || board[1][3] == "O" && board[2][2] == "O" && board[3][1] == "O") {
                        System.out.print(" " + board[1][0] + " " + board[2][0] + " " + board[3][0]);
                        System.out.println();
                        for (int i = 0; i < 3; i++) {
                            if (i == 0) {
                                System.out.print(board[0][1]);
                            } else if (i == 1) {
                                System.out.print(board[0][2]);
                            } else if (i == 2) {
                                System.out.print(board[0][3]);
                            }

                            for (int j = 1; j < 4; j++) {

                                if (board[i + 1][j] == null || board[i + 1][j] == null || board[i + 1][j] == null) {
                                    System.out.print("- ");
                                } else {
                                    System.out.print(board[i + 1][j] + " ");
                                }
                            }
                            System.out.println("");
                        }

                        System.out.println("Player O win");
                        win = true;
                    } else {
                        if (count == 9) {
                            System.out.print(" " + board[1][0] + " " + board[2][0] + " " + board[3][0]);
                            System.out.println();
                            for (int i = 0; i < 3; i++) {
                                if (i == 0) {
                                    System.out.print(board[0][1]);
                                } else if (i == 1) {
                                    System.out.print(board[0][2]);
                                } else if (i == 2) {
                                    System.out.print(board[0][3]);
                                }

                                for (int j = 1; j < 4; j++) {

                                    if (board[i + 1][j] == null || board[i + 1][j] == null || board[i + 1][j] == null) {
                                        System.out.print("- ");
                                    } else {
                                        System.out.print(board[i + 1][j] + " ");
                                    }
                                }
                                System.out.println("");
                            }
                            System.out.println("Drawn!!!!!");
                            win = true;
                        }
                    }
                }

            }
            System.out.println("Bye Bye");

        } catch (Exception e) {

        }

    }

}
